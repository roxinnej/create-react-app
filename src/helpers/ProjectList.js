import ariWebsite from '../assets/psychology-website.png';
import mcmichaelWebsite from '../assets/mcmichael-website.png';
import sandajWebsite from '../assets/cafe-website.png';
import LHCWebsite from '../assets/construction-website.png';
import birthdayWebsite from '../assets/tex-birthday-website.png';

export const projectList = [
    {
        name: "Rachel McMichael Consulting LLC",
        image: mcmichaelWebsite,
        skills: "Wordpress | Divi theme | CSS",
        link: "https://rachelmcmichael.com"
    },
    {
        name: "Ari Kellner Psychology Group",
        image: ariWebsite,
        skills: "Wordpress | Divi theme | CSS",
        link: "https://arikellner.com"
    },
    {
        name: "S & AJ Cafe",
        image: sandajWebsite,
        skills: "Wordpress | Divi theme | CSS",
        link: "https://sandajcafe.com.au"
    }

]