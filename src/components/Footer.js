import React from 'react';
import "../styles/Footer.css";
import SocialMediaIcons from "./SocialMediaIcons";

function Footer() {
  return (
    <div className='footer'>
        <div className='socialMedia'>
          <SocialMediaIcons />
        </div>
        <div class="copyright">
          <p> &copy; 2023 roxroa.com</p>
        </div>
    </div>
  )
}

export default Footer