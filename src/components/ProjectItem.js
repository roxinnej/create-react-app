import React from 'react';
import {useNavigate} from "react-router-dom";

function ProjectItem({image, name, id, skills, link}) {
  const navigate = useNavigate();
  return (

    <div className='projectItem'>
      <img src= {image} className='bgImage' />
      <h4>{name}</h4>
      <p className='skills'>
        {skills}
        <br />
        <a href={link} target="_blank">{link}</a>
      </p>
    </div>
  )
}

export default ProjectItem