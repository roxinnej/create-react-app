import React from 'react';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import EmailIcon from '@material-ui/icons/Email';
import CodeIcon from '@material-ui/icons/Code';

function SocialMediaIcons() {
  return (
    <div>
        <a href="https://www.linkedin.com/in/roxinneroa/" target="_blank"><LinkedInIcon /></a>
        <a href="mailto:roxinnej@gmail.com"><EmailIcon /></a>
        <a href="https://gitlab.com/roxinnej" target="_blank"><CodeIcon /></a>
    </div>
  );
}

export default SocialMediaIcons