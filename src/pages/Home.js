import React from 'react';
import "../styles/Home.css";
import SocialMediaIcons from "../components/SocialMediaIcons";
import roxImage from "../assets/roxinne-roa.png";

function Home() {
  return (
    <>
    
    <div className='home dark-theme green-bg center'>
      <div className='intro'>
        <h2>Roxinne Roa</h2>
        <div className='prompt'> 
          <h4>Front-End Developer</h4>
          <em>I love puzzles and problem-solving.</em>
        </div>
        <SocialMediaIcons />
      </div>
    </div>
      <div className='about section'>
        <div className='column'>
          <div className='image'>
            <img src={roxImage}/>
          </div>
        </div>
        <div className='column more'>
          <p>I had the opportunity to work as a web developer at a fast-paced agency, and I absolutely thrived in that environment! 
            It was exhilarating to be able to create fully-functioning websites within just one work day.</p>
          <p>As I gained more experience and knowledge in the field, I transitioned to a work-from-home position as an in-house web developer, 
            which I enjoyed for six years. It was a great experience, and I truly loved every moment of it.</p>
          <p>Recently, my family and I made a move to a new country, which has led me to embark on an exciting search for the perfect company to join. 
            I am eager to continue developing my career in the tech industry and contribute my skills to a new team.</p>
          <p>Outside of work, my family is my priority, and we love spending quality time together. Road trips are one of our favorite activities, 
            allowing us to explore new places and create lasting memories.</p>
        </div>
      </div>
    </>
  )
}

export default Home;