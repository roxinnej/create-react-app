import React from 'react'
import { useParams } from "react-router-dom";
import { projectList } from "../helpers/ProjectList";

function ProjectDisplay() {
  const { id } = useParams();
  const project = projectList[id];
  return (
    <div className='project'>
      <h1>{project.name}</h1>
      <img src={project.image} />
      <p>Skills: {project.skills}</p>
      <a href={project.url}>{project.link} {project.skills}</a>
      </div>
  )
}

export default ProjectDisplay;