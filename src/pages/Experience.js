import React from 'react';
import {
  VerticalTimeline, 
  VerticalTimelineElement
} from 'react-vertical-timeline-component';
import "react-vertical-timeline-component/style.min.css";
import SchoolIcon from "@material-ui/icons/School";
import WorkIcon from "@material-ui/icons/Work";

function Experience() {
  return (
    <div className='experience section'>
      <h1 className='center'>Experience</h1>
      <VerticalTimeline lineColor="#101617">

        <VerticalTimelineElement 
          className='vertical-timeline-element--work'
          date='2017 - 2023'
          iconStyle={{background: '#182324', color: '#5cffa2'}}
          icon ={<WorkIcon/>}>
            <h3 className='vertical-timeline-element--title'>Website Developer</h3>
            <h5>McMichael Consulting LLC</h5>
            <p>This was a fully-remote position for a business consultation company. The business is based in Arkansas, USA.</p>
            <ul>
              <li>My main responsibility was maintaining the company website www.rachelmcmichael.com, including:</li>
              <ul>
                <li>Making sure the website is up and running daily Weekly site speed check and broken links check Weekly upload of website blog received from writer SEO integration using Yoast plugin</li>
                <li>Fixing website issues especially during downtime</li>
                <li>Created and rebranded the website in 2021</li>
                <li>Migrated the site and all its contents from Squarespace to Wordpress in 2019</li>
              </ul>
              <li>Website design and development for different clients. I worked on:</li>
              <ul>
                <li>branding board creation depending on client's specifications logo creation whenever necessary</li>
                <li>website design and development using branding board</li>
                <li>website platforms include: Wordpress, Squarespace, and Kajabi</li>
              </ul>
              <li>Graphics department assistance through:</li>
              <ul>
                <li>PDF handbooks and workbooks design for coaching classes using Canva, Keynote, and Pages</li>
                <li>Creation of company logo</li>
                <li>Information graphics designs for IG posts and stories</li>
                <li>Facebook cover designs</li>
                <li>Podcast logo design for iTunes</li>
              </ul>
              <li>Assisted in social media presence through:</li>
                <ul>
                  <li>Handling of Instagram account and uploading content</li>
                  <li>Pitching ideas for better Instagram presence</li>
                </ul>
            </ul>
        </VerticalTimelineElement>

        <VerticalTimelineElement 
          className='vertical-timeline-element--work'
          date='2015 - 2017'
          iconStyle={{background: '#182324', color: '#5cffa2'}}
          icon ={<WorkIcon/>}>
            <h3 className='vertical-timeline-element--title'>Website Developer</h3>
            <h5>Platyppines Media Solutions</h5>
            <p>I worked at media solutions agency together with 3 other developers. My focus was on making websites for various international clients and businesses. We used Wordpress - Divi theme, and CSS for customization.</p>
            <ul>
              <li>Designed and developed websites according to each client's branding board, business niche, and content</li>
              <li>Have created a 3-page website in one day</li>
              <li>Worked on tight deadlines with no problem</li>
              <li>Made sure websites look good on mobile and tablet, are responsive, and working on main browsers (Google, Opera, and Safari)</li>
              <li>Created simple logos whenever necessary</li>
              <li>Used Pivotal Tracker to work on tickets for various updates on the website</li>
            </ul>
        </VerticalTimelineElement>

<VerticalTimelineElement 
  className='vertical-timeline-element--work'
  date='2013'
  iconStyle={{background: '#182324', color: '#5cffa2'}}
  icon ={<WorkIcon/>}>
    <h3 className='vertical-timeline-element--title'>Sales Administrator</h3>
    <h5>ABS-C Corporation</h5>
    <ul>
      <li>Led operations monitoring through collaborating with 4 other branches and their respective Sales Administrator</li>
      <li>Supported daily operations through admin management</li>
      <li>Data entry using Microsoft Excel</li>
    </ul>
</VerticalTimelineElement>


        <VerticalTimelineElement 
          className='vertical-timeline-element--education'
          date='2009 - 2013'
          iconStyle={{background: '#182324', color: '#5cffa2'}}
          icon ={<SchoolIcon/>}>
            <h3 className='vertical-timeline-element--title'>Tertiary Education</h3>
            <h5>Mindanao University of Science and Technology</h5>
            <p>Bachelor of Science in Information Technology<br/>
            Graduated Year 2013</p>
        </VerticalTimelineElement>

      </VerticalTimeline>
    </div>
  )
}

export default Experience