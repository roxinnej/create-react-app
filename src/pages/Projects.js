import React from 'react';
import ProjectItem from '../components/ProjectItem';
import { projectList } from "../helpers/ProjectList";
import "../styles/Projects.css";

function Projects() {
  return (
    <div className='projects green-bg section center dark-theme'>
      <h1>My Personal Projects</h1>
      <div className='projectList'>
      {projectList.map((project, index) => {
        return <ProjectItem 
          name = {project.name} 
          image = {project.image} 
          id={index} 
          skills={project.skills} 
          link={project.link} />
      })}
      </div>

    </div>

  )
}

export default Projects